### Inter API

Uma API para geração de dígitos únicos e um CRUD de usuários.

#### Tecnologias

* Java 8
* Spring Boot
* Maven
* H2

#### Ferramentas auxiliares

1. _Swagger_: para gerar a documentação da API.
2. _Postman_: para realizar testes dos endpoints.

#### Execução

* Antes de executar o projeto, os testes podem ser feitos através
do comando:

`mvn test`

* A próxima instrução é responsável pela execução da aplicação:

`mvn spring-boot:run`

* Os endpoints estão disponíveis na URL:

`http://localhost:8080/swagger-ui.html`
