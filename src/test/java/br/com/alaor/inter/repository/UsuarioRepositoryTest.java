package br.com.alaor.inter.repository;

import br.com.alaor.inter.model.DigitoUnico;
import br.com.alaor.inter.model.Usuario;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class UsuarioRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Before
    public void init() {
        Usuario usuario1 = new Usuario("Alaor", "alaor@alaor");
        entityManager.persist(usuario1);
        entityManager.flush();

        Usuario usuario2 = new Usuario("Gustavo", "gustavo@gustavo");
        entityManager.persist(usuario2);
        entityManager.flush();
    }

    @Test
    public void assertThatSizeIsCorrect() {
        List<Usuario> usuarios = usuarioRepository.findAll();
        assertThat(usuarios.size()).isEqualTo(2);
        assertThat(usuarios.get(0).getNome()).isEqualTo("Alaor");
        assertThat(usuarios.get(1).getNome()).isEqualTo("Gustavo");
    }

    @Test
    public void assertThatFindByIdisPresent() {
        Optional<Usuario> usuario = usuarioRepository.findById(1L);
        assertThat(usuario.isPresent());
        assertThat(usuario.get().getNome()).isEqualTo("Alaor");
    }

    @Test
    public void assertThatFindByIdIsNotPresent() {
        Optional<Usuario> usuario = usuarioRepository.findById(5L);
        assertThat(usuario.isPresent()).isEqualTo(false);
    }

}
