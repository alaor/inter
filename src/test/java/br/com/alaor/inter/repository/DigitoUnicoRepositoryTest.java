package br.com.alaor.inter.repository;

import br.com.alaor.inter.model.DigitoUnico;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class DigitoUnicoRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DigitoUnicoRepository digitoUnicoRepository;

    @Before
    public void init() {
        DigitoUnico digitoUnico_1 = new DigitoUnico(9875, 4);
        DigitoUnico digitoUnico_2 = new DigitoUnico(23, 3);
        DigitoUnico digitoUnico_3 = new DigitoUnico(12, 5);

        entityManager.persist(digitoUnico_1);
        entityManager.flush();
        entityManager.persist(digitoUnico_2);
        entityManager.flush();
        entityManager.persist(digitoUnico_3);
        entityManager.flush();
    }

    @Test
    public void assertThatSizeIsCorrect() {
        List<DigitoUnico> digitoUnicoList = digitoUnicoRepository.findAll();
        assertThat(digitoUnicoList.size()).isEqualTo(3);
    }

    @Test
    public void assertThatFindByNandKisPresent() {
        Optional<DigitoUnico> digitoUnico = digitoUnicoRepository.findByNAndK(9875, 4);
        assertThat(digitoUnico.isPresent());
    }

    @Test
    public void assertThatFindByNandKIsNotPresent() {
        Optional<DigitoUnico> digitoUnico = digitoUnicoRepository.findByNAndK(1, 2);
        assertThat(digitoUnico.isPresent()).isEqualTo(false);
    }

}
