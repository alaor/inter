package br.com.alaor.inter.controller;

import br.com.alaor.inter.dto.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(DigitoUnicoController.class)
public class DigitoUnicoControllerTest {

    private static final ObjectMapper om = new ObjectMapper();

    @MockBean
    private DigitoUnicoController digitoUnicoController;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getDigitosUnicos() throws Exception {
        DigitoUnicoDetailDto digitoUnicoDetailDto = new DigitoUnicoDetailDto();
        digitoUnicoDetailDto.setDigitoUnico(123123L);
        digitoUnicoDetailDto.setN(123);
        digitoUnicoDetailDto.setK(2);
        digitoUnicoDetailDto.setDigitoUnico(12L);
        List<DigitoUnicoDetailDto> digitosUnicos = singletonList(digitoUnicoDetailDto);

        given(digitoUnicoController.all()).willReturn(digitosUnicos);

        mockMvc.perform(get("/api/digito-unico/all")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].numero", is(digitoUnicoDetailDto.getNumero())))
                .andExpect(jsonPath("$[0].digitoUnico", is(12)))
                .andExpect(jsonPath("$[0].n", is(123)))
                .andExpect(jsonPath("$[0].k", is(2)));

    }

    @Test
    public void createDigitoUnico() throws Exception {
        DigitoUnicoDetailDto digitoUnicoDetailDto = new DigitoUnicoDetailDto();
        digitoUnicoDetailDto.setDigitoUnico(252525L);
        digitoUnicoDetailDto.setN(25);
        digitoUnicoDetailDto.setK(3);
        digitoUnicoDetailDto.setDigitoUnico(21L);
        when(digitoUnicoController.createDigitoUnico(any(DigitoUnicoSimpleDto.class)))
                .thenReturn(ResponseEntity.ok(digitoUnicoDetailDto));

        mockMvc.perform(post("/api/digito-unico")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"n\": 25, \"k\": 3 }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.n").value(digitoUnicoDetailDto.getN()))
                .andExpect(jsonPath("$.k").value(digitoUnicoDetailDto.getK()))
                .andExpect(jsonPath("$.numero").value(digitoUnicoDetailDto.getNumero()))
                .andExpect(jsonPath("$.digitoUnico").value(digitoUnicoDetailDto.getDigitoUnico()));

    }

    @Test
    public void createDigitoUnicoWithUsuario() throws Exception {
        DigitoUnicoDetailDto digitoUnicoDetailDto = new DigitoUnicoDetailDto();
        digitoUnicoDetailDto.setDigitoUnico(15151515L);
        digitoUnicoDetailDto.setN(15);
        digitoUnicoDetailDto.setK(4);
        digitoUnicoDetailDto.setDigitoUnico(24L);

        when(digitoUnicoController.createDigitoUnicoWithUsuario(any(DigitoUnicoUsuarioPostDto.class)))
                .thenReturn(ResponseEntity.ok(digitoUnicoDetailDto));

        mockMvc.perform(post("/api/digito-unico/usuario")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"n\": 15, \"k\": 4 }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.n").value(digitoUnicoDetailDto.getN()))
                .andExpect(jsonPath("$.k").value(digitoUnicoDetailDto.getK()))
                .andExpect(jsonPath("$.numero").value(digitoUnicoDetailDto.getNumero()))
                .andExpect(jsonPath("$.digitoUnico").value(digitoUnicoDetailDto.getDigitoUnico()));
    }

}





