package br.com.alaor.inter.controller;

import br.com.alaor.inter.dto.UsuarioDetailDto;
import br.com.alaor.inter.dto.UsuarioSimpleDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.mockito.ArgumentMatchers.any;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

    private static final ObjectMapper om = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UsuarioController usuarioController;

    @Autowired
    private ModelMapper modelMapper;

    @Before
    public void init() {
        UsuarioDetailDto usuarioDetailDto = new UsuarioDetailDto();
        usuarioDetailDto.setId(1L);
        usuarioDetailDto.setNome("Alaor");
        usuarioDetailDto.setEmail("alaor.resende@gmail.com");
        when(usuarioController.getUsuarioById(1L)).thenReturn(ResponseEntity.ok(usuarioDetailDto));
    }

    @Test
    public void getUsuarioById() throws Exception {

        mockMvc.perform(get("/api/usuario/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nome", is("Alaor")))
                .andExpect(jsonPath("$.email", is("alaor.resende@gmail.com")));
    }

    @Test
    public void getUsuarios() throws Exception {
        UsuarioDetailDto usuario = new UsuarioDetailDto();
        usuario.setId(1L);
        usuario.setNome("Alaor");
        usuario.setEmail("alaor.resende@gmail.com");
        List<UsuarioDetailDto> usuarios = singletonList(usuario);

        given(usuarioController.all()).willReturn(usuarios);

        mockMvc.perform(get("/api/usuario/all")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].nome", is(usuario.getNome())))
                .andExpect(jsonPath("$[0].email", is(usuario.getEmail())));
    }

    @Test
    public void createUsuario() throws Exception {
        UsuarioDetailDto usuarioDetailDto = new UsuarioDetailDto();
        usuarioDetailDto.setId(1L);
        usuarioDetailDto.setNome("Alaor");
        usuarioDetailDto.setEmail("alaor.resende@gmail.com");
        when(usuarioController.create(any(UsuarioSimpleDto.class))).thenReturn(usuarioDetailDto);

        mockMvc.perform(post("/api/usuario")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"nome\": \"Alaor\", \"email\": \"alaor.resende@gmail.com\" }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.nome").value("Alaor"))
                .andExpect(jsonPath("$.email").value("alaor.resende@gmail.com"));

    }

    @Test
    public void updateUsuario() throws Exception {
        UsuarioDetailDto usuarioDetailDto = new UsuarioDetailDto();
        usuarioDetailDto.setId(1L);
        usuarioDetailDto.setNome("Alaor Updated");
        usuarioDetailDto.setEmail("alaor.resende_updated@gmail.com");

        UsuarioSimpleDto usuarioSimpleDto = new UsuarioSimpleDto();
        usuarioSimpleDto.setNome("Alaor Updated");
        usuarioSimpleDto.setEmail("alaor.resende_updated@gmail.com");
        when(usuarioController.update(1L, usuarioSimpleDto))
                .thenReturn(ResponseEntity.ok(usuarioDetailDto));

        mockMvc.perform(put("/api/usuario/1")
                .content(om.writeValueAsBytes(usuarioSimpleDto))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.nome", is("Alaor Updated")))
                .andExpect(jsonPath("$.email", is("alaor.resende_updated@gmail.com")));
    }

    @Test
    public void deleteUsuario() throws Exception {

        Map<String, Boolean> map = new HashMap<String, Boolean>();
        map.put("removido", Boolean.TRUE);
        when(usuarioController.delete(1L)).thenReturn(map);

        mockMvc.perform(delete("/api/usuario/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.removido", is(Boolean.TRUE)));

    }

}
