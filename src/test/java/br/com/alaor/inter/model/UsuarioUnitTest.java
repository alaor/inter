package br.com.alaor.inter.model;

import br.com.alaor.inter.model.Usuario;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UsuarioUnitTest {

    @Test
    public void whenCalledGetName_thenCorrect() {
        Usuario usuario = new Usuario("Alaor", "alaor.resende@gmail.com");
        assertThat(usuario.getNome()).isEqualTo("Alaor");
    }

    @Test
    public void whenCalledGetEmail_thenCorrect() {
        Usuario usuario = new Usuario("Alaor", "alaor.resende@gmail.com");
        assertThat(usuario.getEmail()).isEqualTo("alaor.resende@gmail.com");
    }

    @Test
    public void whenCalledSetName_thenCorrect() {
        Usuario usuario = new Usuario("Alaor", "alaor.resende@gmail.com");
        usuario.setNome("Paulo");
        assertThat(usuario.getNome()).isEqualTo("Paulo");
    }

    @Test
    public void whenCalledSetEmail_thenCorrect() {
        Usuario usuario = new Usuario("Alaor", "alaor.resende@gmail.com");
        usuario.setEmail("paulo.resende@gmail.com");
        assertThat(usuario.getEmail()).isEqualTo("paulo.resende@gmail.com");
    }

}
