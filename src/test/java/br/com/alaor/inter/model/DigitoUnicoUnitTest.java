package br.com.alaor.inter.model;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DigitoUnicoUnitTest {

    @Test
    public void whenCalledGetNumero_thenCorrect() {
        DigitoUnico digitoUnico = new DigitoUnico(9875, 4);
        assertThat(digitoUnico.getNumero()).isEqualTo(9875987598759875L);
    }

    @Test
    public void whenCalledGetDigitoUnico_thenCorrect() {
        DigitoUnico digitoUnico = new DigitoUnico(9875, 4);
        assertThat(digitoUnico.getDigitoUnico()).isEqualTo(116L);
    }

    @Test
    public void whenCalledSetNumero_thenCorrect() {
        DigitoUnico digitoUnico = new DigitoUnico(9875, 4);
        digitoUnico.setNumero(12345L);
        assertThat(digitoUnico.getNumero()).isEqualTo(12345L);
    }

    @Test
    public void whenCalledSetDigitoUnico_thenCorrect() {
        DigitoUnico digitoUnico = new DigitoUnico(9875, 4);
        digitoUnico.setDigitoUnico(12345L);
        assertThat(digitoUnico.getDigitoUnico()).isEqualTo(12345L);
    }

}
