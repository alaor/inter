package br.com.alaor.inter.repository;

import br.com.alaor.inter.model.DigitoUnico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico, Long> {

    Optional<DigitoUnico> findByNAndK(Integer n, Integer k);

    List<DigitoUnico> findByUsuariosId(Long id);

}



