package br.com.alaor.inter.controller;

import br.com.alaor.inter.dto.DigitoUnicoDetailDto;
import br.com.alaor.inter.dto.DigitoUnicoSimpleDto;
import br.com.alaor.inter.dto.DigitoUnicoUsuarioPostDto;
import br.com.alaor.inter.exception.ResourceNotFoundException;
import br.com.alaor.inter.model.DigitoUnico;
import br.com.alaor.inter.model.Usuario;
import br.com.alaor.inter.repository.DigitoUnicoRepository;
import br.com.alaor.inter.repository.UsuarioRepository;
import br.com.alaor.inter.service.DigitoUnicoCache;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/digito-unico")
public class DigitoUnicoController {

    @Autowired
    private DigitoUnicoRepository digitoUnicoRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private DigitoUnicoCache cache;

    @Autowired
    private ModelMapper mapper;

    @GetMapping("/cache")
    @ApiOperation(value = "Listar todos os dígitos armazenados em cache.")
    public Set<Map.Entry<String, DigitoUnico>> cache(){
        return cache.getCache().entrySet();
    }

    @GetMapping("/all")
    @ApiOperation(value = "Listar todos os dígitos únicos.")
    public List<DigitoUnicoDetailDto> all() {
        return digitoUnicoRepository.findAll().stream()
                .map(digitoUnico -> mapper.map(digitoUnico, DigitoUnicoDetailDto.class)).collect(Collectors.toList());
    }

    @GetMapping("/list-by-usuario/{usuarioId}")
    @ApiOperation(value = "Listar todos os dígitos associados a um usuário.")
    public List<DigitoUnicoDetailDto> listByUsuario(@PathVariable(value = "usuarioId") Long usuarioId) {
        return digitoUnicoRepository.findByUsuariosId(usuarioId).stream()
                .map(digitoUnico -> mapper.map(digitoUnico, DigitoUnicoDetailDto.class)).collect(Collectors.toList());
    }

    @PostMapping()
    @ApiOperation(value = "Criar um novo dígito único.")
    public ResponseEntity<DigitoUnicoDetailDto> createDigitoUnico(@Valid @RequestBody DigitoUnicoSimpleDto digitoUnicoDto) {

        DigitoUnico digitoUnico;
        String key = cache.generateKey(digitoUnicoDto.getN(), digitoUnicoDto.getK());
        if (cache.isEmpty()) {
            digitoUnico = new DigitoUnico(digitoUnicoDto.getN(), digitoUnicoDto.getK());
            cache.add(key, digitoUnico);
        } else {
            digitoUnico = cache.get(key);
            if (digitoUnico == null) {
                digitoUnico = new DigitoUnico(digitoUnicoDto.getN(), digitoUnicoDto.getK());
                cache.add(key, digitoUnico);
            }
        }

        digitoUnicoRepository.save(digitoUnico);
        return ResponseEntity.ok().body(mapper.map(digitoUnico, DigitoUnicoDetailDto.class));

    }

    @SneakyThrows
    @PostMapping("/usuario")
    @ApiOperation(value = "Criar um novo dígito único e associá-lo a um usuário existente.")
    public ResponseEntity<DigitoUnicoDetailDto> createDigitoUnicoWithUsuario(
            @Valid @RequestBody DigitoUnicoUsuarioPostDto digitoUnicoDto) {

        DigitoUnico digitoUnico;
        String key = cache.generateKey(digitoUnicoDto.getN(), digitoUnicoDto.getK());
        Usuario usuario = usuarioRepository.findById(digitoUnicoDto.getUsuarioId()).orElseThrow(
                () -> new ResourceNotFoundException("Usuário não encontrado para o id: " +
                        digitoUnicoDto.getUsuarioId()));

        if (cache.isEmpty()) {
            digitoUnico = new DigitoUnico(digitoUnicoDto.getN(), digitoUnicoDto.getK());
            cache.add(key, digitoUnico);
        } else {
            digitoUnico = cache.get(key);
            if (digitoUnico == null) {
                Optional<DigitoUnico> findDigitoUnico = digitoUnicoRepository
                        .findByNAndK(digitoUnicoDto.getN(), digitoUnicoDto.getK());
                if (findDigitoUnico.isPresent()) {
                    digitoUnico = findDigitoUnico.get();
                } else {
                    digitoUnico = new DigitoUnico(digitoUnicoDto.getN(), digitoUnicoDto.getK());
                }
            }
        }

        digitoUnico.getUsuarios().add(usuario);
        digitoUnicoRepository.save(digitoUnico);
        return ResponseEntity.ok().body(mapper.map(digitoUnico, DigitoUnicoDetailDto.class));

    }



}
