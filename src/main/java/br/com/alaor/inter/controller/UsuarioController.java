package br.com.alaor.inter.controller;

import br.com.alaor.inter.config.DateUtil;
import br.com.alaor.inter.dto.UsuarioDetailDto;
import br.com.alaor.inter.dto.UsuarioSimpleDto;
import br.com.alaor.inter.exception.BadRequestException;
import br.com.alaor.inter.exception.ErrorDetails;
import br.com.alaor.inter.exception.ResourceNotFoundException;
import br.com.alaor.inter.model.Usuario;
import br.com.alaor.inter.repository.UsuarioRepository;
import br.com.alaor.inter.service.RsaService;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {

    @Autowired
    private RsaService rsaService;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private ModelMapper mapper;

    @GetMapping("/all")
    @ApiOperation(value = "Listar todos os usuários.")
    public List<UsuarioDetailDto> all() {
        return usuarioRepository.findAll().stream()
                .map(usuario -> mapper.map(usuario, UsuarioDetailDto.class)).collect(Collectors.toList());
    }

    @SneakyThrows
    @GetMapping("/{id}")
    @ApiOperation(value = "Listar usuário por id.")
    public ResponseEntity<UsuarioDetailDto> getUsuarioById(@PathVariable(value = "id") Long usuarioId) {

        Usuario usuario = usuarioRepository.findById(usuarioId).orElseThrow(
                () -> new ResourceNotFoundException("Usuário não encontrado para o id: " + usuarioId));

        return ResponseEntity.ok().body(mapper.map(usuario, UsuarioDetailDto.class));
    }

    @PostMapping()
    @ApiOperation(value = "Criar um novo usuário.")
    public UsuarioDetailDto create(@Valid @RequestBody UsuarioSimpleDto usuarioPostDto) {
        Usuario usuario = mapper.map(usuarioPostDto, Usuario.class);
        return mapper.map(usuarioRepository.save(usuario), UsuarioDetailDto.class);
    }

    @SneakyThrows
    @PutMapping("/{id}")
    @ApiOperation(value = "Atualizar os dados de um usuário.")
    public ResponseEntity<UsuarioDetailDto> update(@PathVariable(value = "id") Long usuarioId,
                                                      @Valid @RequestBody UsuarioSimpleDto usuarioEditar) {

        Usuario usuario = usuarioRepository.findById(usuarioId).orElseThrow(
                () -> new ResourceNotFoundException("Usuário não encontrado para o id: " + usuarioId));

        usuario.setEmail(usuarioEditar.getEmail());
        usuario.setNome(usuarioEditar.getNome());
        return ResponseEntity.ok(mapper.map(usuarioRepository.save(usuario), UsuarioDetailDto.class));
    }

    @SneakyThrows
    @DeleteMapping("/{id}")
    @ApiOperation(value = "Remover um usuário por id.")
    public Map<String, Boolean> delete(@PathVariable(value = "id") Long usuarioId) {

        Usuario usuario = usuarioRepository.findById(usuarioId).orElseThrow(
                () -> new ResourceNotFoundException("Usuário não encontrado para o id: " + usuarioId));

        usuario.getDigitosUsuario().forEach(digitoUnico -> digitoUnico.removeUsuario(usuario));

        usuarioRepository.delete(usuario);
        Map <String, Boolean> response = new HashMap<>();
        response.put("removido", Boolean.TRUE);
        return response;
    }

    @SneakyThrows
    @PutMapping("/{id}/encrypt")
    @ApiOperation(value = "Criptografar os dados do usuário.")
    public ResponseEntity criptografaUsuario(@PathVariable(value = "id") Long usuarioId) {

        Usuario usuario = usuarioRepository.findById(usuarioId).orElseThrow(
                () -> new ResourceNotFoundException("Usuário não encontrado para o id: " + usuarioId));

        if(!rsaService.userHasKeyPair(usuario)) {
            rsaService.generateUserKeyPair(usuario);
            usuario = rsaService.encrypt(usuario);
            return ResponseEntity.ok(mapper.map(usuarioRepository.save(usuario), UsuarioDetailDto.class));
        }

        throw new BadRequestException("Falha ao criptografar os dados do usuário: " + usuarioId);

    }

    @SneakyThrows
    @PutMapping("/{id}/decrypt")
    @ApiOperation(value = "Descriptografar os dados do usuário.")
    public ResponseEntity decriptografaUsuario(@PathVariable(value = "id") Long usuarioId) {

        Usuario usuario = usuarioRepository.findById(usuarioId).orElseThrow(
                () -> new ResourceNotFoundException("Usuário não encontrado para o id: " + usuarioId));

        if(rsaService.userHasKeyPair(usuario)) {
            usuario = rsaService.decrypt(usuario);
            return ResponseEntity.ok(mapper.map(usuarioRepository.save(usuario), UsuarioDetailDto.class));
        }

        throw new BadRequestException("Falha ao descriptografar os dados do usuário: " + usuarioId);

    }

}
