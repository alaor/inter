package br.com.alaor.inter.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Entity
@Table(name = "digito_unico")
@NoArgsConstructor
@Data
public class DigitoUnico implements Serializable {

    public DigitoUnico(Integer n, Integer k) {
        this.n = n;
        this.k = k;
        this.numero = gerarNumero(this.n, this.k);
        this.digitoUnico = gerarDigitoUnico(this.numero);
    }

    @Id
    @ApiModelProperty(value = "Número gerado entre 'n' e 'k' também utilizado com o Id da entidade.")
    @NotNull(message = "Número não pode ser nulo.")
    private Long numero;

    @ApiModelProperty(value = "Número para base do cálculo.")
    @NotNull(message = "Propriedade N não pode ser nula.")
    private Integer n;

    @ApiModelProperty(value = "Número de repetições para o valor de 'n'.")
    @NotNull(message = "Propriedad K não pode ser nula.")
    private Integer k;

    @ApiModelProperty(value = "Dígito único gerado a partir da propriedade 'Número'.")
    @NotNull(message = "Propriedade 'digitoUnico' não pode ser nula.")
    private Long digitoUnico;

    @JsonBackReference
    @ManyToMany
    @JoinTable(
            name = "usuario_digitos",
            joinColumns = @JoinColumn(name = "numero"),
            inverseJoinColumns = @JoinColumn(name = "id"))
    Set<Usuario> usuarios = new HashSet<>();

    public void removeUsuario(Usuario usuario) {
        this.usuarios.remove(usuario);
        usuario.getDigitosUsuario().remove(this);
    }

    @SneakyThrows
    private Long gerarNumero(Integer numero, Integer repeticoesNumero) {

        if (numero == null || repeticoesNumero == null)
            throw new Exception("Número não pode ser nulo.");

        StringBuilder sbNum = new StringBuilder();
        if (repeticoesNumero == 0) {
            return numero.longValue();
        } else {
            for (int i = 0; i < repeticoesNumero; i++) {
                sbNum.append(numero);
            }
            return Long.parseLong(sbNum.toString());
        }
    }

    @SneakyThrows
    private Long gerarDigitoUnico(Long numero) {

        if (numero == null)
            throw new Exception("Número não pode ser nulo.");

        if (numero.toString().length() == 1) {
            return numero;
        } else {
            List<String> strings = numero.toString().chars()
                    .mapToObj(c -> Character.toString((char)c))
                    .collect(Collectors.toList());

            LongStream longStream = strings.stream().mapToLong(Long::parseLong);
            return longStream.summaryStatistics().getSum();
        }

    }

    @Override
    public String toString() {
        return "DigitoUnico{" +
                "numero=" + numero +
                ", n=" + n +
                ", k=" + k +
                ", digitoUnico=" + digitoUnico +
                '}';
    }
}
