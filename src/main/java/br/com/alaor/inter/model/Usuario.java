package br.com.alaor.inter.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "usuario")
@Data
@NoArgsConstructor
public class Usuario implements Serializable {

    public Usuario(String nome, String email) {
        this.nome = nome;
        this.email = email;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "Id do usuário.")
    @NotNull(message = "Id não pode ser nulo.")
    private Long id;

    @ApiModelProperty(value = "Nome do usuário.")
    @NotNull(message = "Nome do usuário não pode ser nulo.")
    @Size(max = 500)
    private String nome;

    @ApiModelProperty(value = "E-mail do usuário.")
    @NotNull(message = "E-mail do usuário não pode ser nulo.")
    @Size(max = 500)
    private String email;

    @ManyToMany(mappedBy = "usuarios")
    Set<DigitoUnico> digitosUsuario = new HashSet<>();

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Usuario other = (Usuario) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }


}
