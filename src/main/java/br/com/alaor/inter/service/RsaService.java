package br.com.alaor.inter.service;

import java.io.IOException;
import java.security.*;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;

import br.com.alaor.inter.model.Usuario;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RsaService {

    private static final String ALGORITMO = "RSA";
    private static final String CHARSET = "UTF-8";

    Map<Usuario, KeyPair> keys = new HashMap<>();

    public void generateUserKeyPair(Usuario usuario) throws Exception {
        log.info("Generates a keyPair for Usuario");
        KeyPairGenerator generator = KeyPairGenerator.getInstance(ALGORITMO);
        generator.initialize(2048, new SecureRandom());
        KeyPair pair = generator.generateKeyPair();
        keys.put(usuario, pair);
    }

    public boolean userHasKeyPair(Usuario usuario) {
        log.info("Check if Usuario already has a KeyPair");
        return keys.get(usuario) != null;
    }

    public Usuario encrypt(Usuario usuario) {
        log.info("Encrypt Usuario data");
        PublicKey pk = keys.get(usuario).getPublic();
        usuario.setNome(doCrypt(usuario.getNome(), pk));
        usuario.setEmail(doCrypt(usuario.getEmail(), pk));
        return usuario;
    }

    @SneakyThrows
    private String doCrypt(String value, PublicKey pk) {
        Cipher encryptCipher = Cipher.getInstance(ALGORITMO);
        encryptCipher.init(Cipher.ENCRYPT_MODE, pk);
        return Base64.getEncoder().encodeToString(encryptCipher.doFinal(value.getBytes(CHARSET)));
    }

    public Usuario decrypt(Usuario usuario) {
        log.info("Decrypt Usuario data");
        PrivateKey pk = keys.get(usuario).getPrivate();
        usuario.setNome(doDecrypt(usuario.getNome(), pk));
        usuario.setEmail(doDecrypt(usuario.getEmail(), pk));
        keys.remove(usuario);
        return usuario;
    }

    @SneakyThrows
    private String doDecrypt(String value, PrivateKey pk) {
        Cipher decriptCipher = Cipher.getInstance("RSA");
        decriptCipher.init(Cipher.DECRYPT_MODE, pk);
        return new String(decriptCipher.doFinal(Base64.getDecoder().decode(value)), CHARSET);
    }

}

