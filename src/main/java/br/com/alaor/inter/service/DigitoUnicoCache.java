package br.com.alaor.inter.service;

import br.com.alaor.inter.model.DigitoUnico;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
@Slf4j
@Getter
public class DigitoUnicoCache {

    private final int maxSizeCache = 10;
    private final LinkedHashMap<String, DigitoUnico> cache = new LinkedHashMap<String, DigitoUnico>();

    public void add(String key, DigitoUnico value) {
        log.info("### Add cache method, the cache size is: " + cache.size());
        if (cache.size() == maxSizeCache) {
            log.warn("### WARNING: Cache needs to remove the first entry item to add a new value!");
            String keyToRemove = cache.keySet().iterator().next();
            log.info("### First key to remove: " + keyToRemove);
            cache.remove(keyToRemove);
            log.info("### New cache size after remove the first entry: " + cache.size());
        }
        log.info("### Adding new key: " + key + " value: " + value.toString() + " to cache!");
        cache.put(key, value);
        log.info("#### New cache size: " + cache.size());
        log.info("### Cache values: " + cache.toString());
        log.info("### End add method ###");
    }

    public DigitoUnico get(String key) {
        log.info("Cache method get key: " + key + " value in cache: " + cache.get(key));
        DigitoUnico digitoUnico = cache.get(key);
        return digitoUnico;
    }

    public boolean isEmpty() {
        return cache.isEmpty();
    }

    public String generateKey(Integer n, Integer k) {
        return n.toString() + "-" + k.toString();
    }

}
