package br.com.alaor.inter.dto;

import lombok.Data;

import java.util.Set;

@Data
public class UsuarioDetailDto {

    private Long id;
    private String nome;
    private String email;
    private Set<DigitoUnicoDetailDto> digitosUsuario;

}
