package br.com.alaor.inter.dto;

import lombok.Data;

@Data
public class UsuarioSimpleDto {

    private String nome;
    private String email;

}
