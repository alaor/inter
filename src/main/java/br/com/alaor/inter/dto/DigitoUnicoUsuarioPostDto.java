package br.com.alaor.inter.dto;

import lombok.Data;

@Data
public class DigitoUnicoUsuarioPostDto {

    private Long usuarioId;
    private Integer n;
    private Integer k;

}
