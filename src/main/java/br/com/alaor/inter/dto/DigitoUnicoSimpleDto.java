package br.com.alaor.inter.dto;

import lombok.Data;

@Data
public class DigitoUnicoSimpleDto {

    private Integer n;
    private Integer k;

}
