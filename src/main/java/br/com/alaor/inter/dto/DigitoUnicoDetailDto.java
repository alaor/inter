package br.com.alaor.inter.dto;

import lombok.Data;

@Data
public class DigitoUnicoDetailDto {

    private Long numero;
    private Integer n;
    private Integer k;
    private Long digitoUnico;


}
